#ifndef SYNC_H
#define SYNC_H

#include <stdio.h>
#include <errno.h>
#include <poll.h>

static inline int sync_wait(int num, int time)
{
    struct pollfd work;
    int result;

    if (num < 0) {
        errno = EINVAL;
        return -1;
    }

    work.fd = num;
    work.events = POLLIN;

    do {
        result = poll(&work, 1, time);
        if (result > 0) {
            if (work.revents & (POLLERR | POLLNVAL)) {
                errno = EINVAL;
                return -1;
            }
            return 0;
        } else if (result == 0) {
            errno = ETIME;
            return -1;
        }
    } while (result == -1 && (errno == EINTR || errno == EAGAIN));

    return result;
}

#endif